import React from "react";

class WarningSpan extends React.Component {

    render () {
        const style = {
            color: 'red',
            marginTop: '1em',
            display: 'none'
        }
        return (
            <>
                <span style={style}>{this.props.children}</span>
            </>
        );
    }
}

export default WarningSpan;