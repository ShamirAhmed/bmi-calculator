import React from "react";

class Button extends React.Component {
    // saveData() {
    //     const weight = document.querySelector('#weight');
    //     const height = document.querySelector('#height');

    //     if (flag === true) {
    //         const date = new Date();
    //         if (window.localStorage.getItem('date') === null || window.localStorage.getItem('weight') === null || window.localStorage.getItem('height') === null) {
    //             window.localStorage.setItem('date', `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`);
    //             window.localStorage.setItem('weight', weight.value);
    //             window.localStorage.setItem('height', height.value);
    //         } else {
    //             window.localStorage.setItem('date', Array(window.localStorage.getItem('date')).concat(`${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`));
    //             window.localStorage.setItem('weight', Array(window.localStorage.getItem('weight')).concat(weight.value));
    //             window.localStorage.setItem('height', Array(window.localStorage.getItem('height')).concat(height.value));
    //         }
    //         const showBmi = document.querySelector('#showBmi');
    //         const bmi = ((weight.value / height.value / height.value) * 10000).toFixed(2)
    //         showBmi.querySelector('span').innerHTML = bmi;
    //         const bmiResult = showBmi.querySelector('h3 span');
    //         if (bmi >= 18.5 && bmi <= 25) {
    //             bmiResult.innerHTML = 'Normal'
    //         } else if (bmi < 18.5) {
    //             bmiResult.innerHTML = 'Under Weight'
    //         } else if (bmi > 25 && bmi <= 30) {
    //             bmiResult.innerHTML = 'Over Weight'
    //         } else {
    //             bmiResult.innerHTML = 'Obese'
    //         }

    //         showBmi.style = 'display:block'
    //     }
    // }

    render() {
        return (
            <>
                <button type={this.props.type} onClick={this.saveData}>{this.props.children}</button>
            </>
        );
    }
}

export default Button;