import React from "react";

class ChartLabels extends React.Component {

    render() {
        const style = {
            '--color': this.props.color
        }
        return (
            <>
                <label className="chart-label" style={style}>{this.props.children}</label>
            </>
        );
    }
}

export default ChartLabels;