import React from 'react';
import WarningSpan from './WarningSpan';

class Input extends React.Component {

    numbersOnly(event) {
        event.target.value = event.target.value.replace(/[^\d.]/g, '');
    }

    render() {
        return (
            <>
                <div className='label-with-input'>
                    <label>{this.props.children}</label>
                    <input type='text' id={this.props.id} onChange={this.props.type === 'text' ? '' : this.numbersOnly} />
                    <WarningSpan>{this.props.warning}</WarningSpan>
                </div>
            </>
        );
    }
}

export default Input;