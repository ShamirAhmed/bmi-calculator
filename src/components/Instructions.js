import React from "react";

class Instructions extends React.Component {

    close(event) {
        event.target.parentElement.style = 'display:none';
    }

    render () {
        return (
                <div className="instructions">
                    <h1>Who shouldn't use a BMI calculator</h1>
                    <ul>
                        <li>Muscle builders</li>
                        <li>Long distance athletes</li>
                        <li>Pregnant women</li> 
                        <li>The elderly or young children</li>
                    </ul>
                    <p>This is because BMI does not take into account whether the weight is carried as muscle or fat, just the number. Those with a higher muscle mass, such as athletes, may have a high BMI but not be at greater health risk. Those with a lower muscle mass, such as children who have not completed their growth or the elderly who may be losing some muscle mass may have a lower BMI. During pregnancy and lactation, a woman's body composition changes, so using BMI is not appropriate.</p>
                    <button onClick={this.close}>close</button>
                </div>
        );
    }
}

export default Instructions;