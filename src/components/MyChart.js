import React from "react";
import Chart from 'chart.js/auto';
import {Bar} from 'react-chartjs-2';

class MyChart extends React.Component {

    render() {
        const chart = this.props.data;
        const data = {
            labels: chart.name,
            datasets: [{
                data: chart.bmi,
                label: "Body Mass Index (BMI)",
                borderColor: "#3e95cd",
                backgroundColor: chart.bmi.map((bmiData) => {
                    if (bmiData >= 18.5 && bmiData <= 25) {
                        return 'green';
                    } else if (bmiData < 18.5) {
                        return 'yellow';
                    } else if (bmiData > 25 && bmiData <= 30) {
                        return 'darkorange';
                    } else {
                        return 'red';
                    }
                }),
                fill: true
            },
            ]
        }

        const options= {
            scales: {
                y: {
                    grid: {
                        color: '#fff'
                    }
                },
                x: {
                    grid: {
                        color: '#fff'
                    }
                }
            },
            plugins: {
                legend: {
                    display: false,
                }
            }
        };
        return (
            <>
                <div className="chart-div">
                    <Bar data={data} options={options}/>
                </div>
            </>
        );
    }
}

export default MyChart;