import './App.css';
import Input from './components/Input';
import Header from './components/Header';
import Button from './components/Button';
import MyChart from './components/MyChart';
import ChartLabels from './components/ChartLabels';
import Instructions from './components/Instructions';
import React from 'react';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      name: [],
      weight: [],
      height: [],
      bmi: [],
      result: [],
    }
    this.style = {
      display: 'none'
    }
  }

  save = (event) => {
    event.preventDefault();
    const name = event.target.name.value.trim();
    const weight = event.target.weight.value.trim();
    const height = event.target.height.value.trim();

    let flag = true;

    if (name === '') {
      flag = false;
      event.target.name.style.border = '2px solid red'
      event.target.name.nextElementSibling.style.display = 'block';
    } else {
      event.target.name.style.border = 'none'
      event.target.name.nextElementSibling.style.display = 'none';
    }

    if (weight === '') {
      flag = false;
      event.target.weight.style.border = '2px solid red';
      event.target.weight.nextElementSibling.style.display = 'block';
    } else {
      event.target.weight.style.border = 'none';
      event.target.weight.nextElementSibling.style.display = 'none';
    }

    if (height === '') {
      flag = false;
      event.target.height.style.border = '2px solid red';
      event.target.height.nextElementSibling.style.display = 'block';
    } else {
      event.target.height.style.border = 'none';
      event.target.height.nextElementSibling.style.display = 'none';
    }

    if (flag === true) {
      const bmi = ((weight / height / height) * 10000).toFixed(2);

      const inputName = this.state.name.concat(name);
      const inputWeight = this.state.weight.concat(parseFloat(weight));
      const inputHeight = this.state.height.concat(parseFloat(height));
      const inputBMI = this.state.bmi.concat(parseFloat(bmi));

      let result;

      if (bmi >= 18.5 && bmi <= 25) {
        result = 'Normal'
      } else if (bmi < 18.5) {
        result = 'Under Weight'
      } else if (bmi > 25 && bmi <= 30) {
        result = 'Over Weight'
      } else {
        result = 'Obese'
      }

      const inputResult = this.state.result.concat(result);

      this.setState({
        name: inputName,
        weight: inputWeight,
        height: inputHeight,
        bmi: inputBMI,
        result: inputResult
      });
      this.style = {
        display: 'block'
      }
      event.target.name.value = '';
      event.target.weight.value = '';
      event.target.height.value = '';
    }
  }

  render() {
    return (
      <>
        <Instructions />
        <Header>BMI Calculator</Header>
        <form onSubmit={this.save}>
          <div className='input-section'>
            <Input type='text' id='name' warning='Please Enter Your Name'>Name</Input>
            <Input id='weight' warning='Please Enter the Weigth'>Weight(in kg)</Input>
            <Input id='height' warning='Please Enter the Height'>Height(in cm)</Input>
          </div>
          <div className='button-container'>
            <Button>Calculate BMI</Button>
          </div>
        </form>
        <div id='showBmi' style={this.style}>
          <h2>Your BMI is <span>{this.state.bmi[this.state.bmi.length - 1]}</span></h2>
          <h3>Your are <span>{this.state.result[this.state.result.length - 1]}</span></h3>
        </div>
        <div className='charts'>
          <span>
            <ChartLabels color='yellow'>Under Weight</ChartLabels>
            <ChartLabels color='green'>Normal</ChartLabels>
            <ChartLabels color='orange'>Over Weight</ChartLabels>
            <ChartLabels color='red'>Obese</ChartLabels>
          </span>
          <MyChart data={this.state} id='myChart2' type='bar' />
          <label>Body Mass Index</label>
        </div>

      </>
    );
  }
}

export default App;
